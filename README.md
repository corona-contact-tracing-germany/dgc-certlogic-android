# EU Digital COVID Certificate - Certlogic Android *light **for reproducible builds***

<p align="center">
  <a href="#about">About</a> •
  <a href="#building">Building</a> •
  <a href="#how-to-contribute">Contribute</a> •
  <a href="#contributors">Contributors</a> •
  <a href="#licensing">Licensing</a>
</p>

## About
This repo is a mod of the [upstream repo][upstream].
It produces a dependency for [CCTG](https://github.com/corona-warn-app/cwa-app-android)'s [reproducible builds](https://codeberg.org/corona-contact-tracing-germany/cwa-android/issues/156) to use.

## Building
* First, you need to init the submodule: `git submodule update --init --recursive`
* Run `./gradlew clean buildAllArtifacts`
* Copy the folder `local-maven-repo` under `<root>/build/` into your project
* In your main project, wire up the local maven repo and include the dependency in your module, e.g.:

```groovy
repositories {
    maven { url "../local-maven-repo" } // If `local-maven-repo` is in your projects root folder
}

dependencies {
    implementation 'dgca.verifier.app:dgc-certlogic-android-light:+'
    // ...
}
```

* Or use dependency substitution to build the project at compile-time of your main project

## How to contribute  

We recommend contributing [upstream][upstream] or at [upstream's upstream][upupstream].

## Contributors  

Our commitment to open source means that we are enabling - in fact encouraging - all interested parties to contribute and become part of its developer community.

## Licensing

Copyright (C) 2021 T-Systems International GmbH, SAP SE and all other contributors

Licensed under the **Apache License, Version 2.0** (the "License"); you may not use this file except in compliance with the License.

You may obtain a copy of the License at https://www.apache.org/licenses/LICENSE-2.0.

Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" 
BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the [LICENSE](./LICENSE) for the specific 
language governing permissions and limitations under the License.

[upstream]: https://github.com/corona-warn-app/dgc-certlogic-android
[upupstream]: https://github.com/eu-digital-green-certificates/dgc-certlogic-android
